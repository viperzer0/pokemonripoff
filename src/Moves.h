#pragma once
#include "Cursor.h"
#include <string>
#include <iostream>

//Puts the > before the currently selected move name and removes it when the
//cursor is moved.
class Moves{
    private:
        std::string move0;
        std::string move1;
        std::string move2;
        std::string move3;

        Cursor cursor;
    public:
        void setMove(int index, std::string move);

        std::string getMove(int index);

        void cursorMove(Directions direction);
        
        void updateText();

        int getCursorPosition();
};

        
    

