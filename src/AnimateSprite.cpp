#include "AnimateSprite.h"
#include <iostream>

void AnimateSprite::setTexture(sf::Texture &texture){
    this->texture = texture;
    sprite.setTexture(texture);
}

void AnimateSprite::addRect(sf::IntRect src){
    srcs.emplace_back(src.left, src.top, src.width, src.height);
}

void AnimateSprite::resetRects(){
    srcs.clear();
}

void AnimateSprite::setDest(sf::IntRect dest){
    this->dest=dest;
    sprite.setPosition(sf::Vector2f(dest.left,dest.top));
    //sprite.setScale(sf::Vector2f(dest.width/srcs.at(currentSrc).width,dest.height/srcs.at(currentSrc).height)); May need to add this back in at some point?
}

sf::Sprite AnimateSprite::getSprite(){
    return sprite;
}

void AnimateSprite::nextFrame(){
    currentSrc++;
    if(currentSrc>=countFrames()) currentSrc=0;
    sprite.setTextureRect(srcs[currentSrc]);
}

void AnimateSprite::setFrame(int frameNum){
    if(frameNum>=0&&frameNum<countFrames()){
        sprite.setTextureRect(srcs[frameNum]);
        currentSrc=frameNum;
    }
}

int AnimateSprite::countFrames(){
    return srcs.size();
}

void AnimateSprite::updateSprite(){
    sprite.setTextureRect(srcs[currentSrc]);
}

int AnimateSprite::getFrame(){
    return currentSrc;
}
