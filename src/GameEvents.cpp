#include "Game.h"

void Game::events(){
    sf::Event event;

    while(window.pollEvent(event))
    {
        if(event.type==sf::Event::Closed)
        running=false;

        if(event.type==sf::Event::KeyPressed)
        {
            //enum
            if(event.key.code == sf::Keyboard::Up)
                moves.cursorMove(Directions::UP);
            if(event.key.code == sf::Keyboard::Right)
                moves.cursorMove(Directions::RIGHT);
            if(event.key.code == sf::Keyboard::Down)
                moves.cursorMove(Directions::DOWN);
            if(event.key.code == sf::Keyboard::Left)
                moves.cursorMove(Directions::LEFT);
            if(event.key.code == sf::Keyboard::Return){
                if(state==GameStates::Title||state==GameStates::Title_Text){
                    state=GameStates::P_Turn;
                    break;
                }
                if(state==GameStates::P_Turn){ //Only allow if it's players turn
                    playerCry.play();
                    status.setString(player->getName()+" uses "+player->getMoveName(moves.getCursorPosition())+"!");//Big update up ahead!
                    player->move(enemy,moves.getCursorPosition());
                    statusClock.restart();
                    state = GameStates::P_Wait;
                }
            }
        }
    }
}
