#pragma once

//List of flags
enum class Flags{NOMUSIC=1,NOSOUND=2};

inline int operator |(Flags a, Flags b)
{
    return (static_cast<int>(a) | static_cast<int>(b));
}

inline int operator &(int a, Flags b)
{
    return a & static_cast<int>(b);
}

inline int operator |(int a, Flags b)
{
    return a | static_cast<int>(b);
}
inline int& operator |=(int& a, Flags b)
{
    return a = a|b;
}




