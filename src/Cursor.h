#pragma once
#include <cstdlib>

 enum class Directions{UP,RIGHT,DOWN,LEFT}; //Compass directions FYI

//Keeps track of cursor position
class Cursor{
    private:
        int position;

    public:
        int getPosition();
        void moveDir(Directions direction);
        void moveUp();
        void moveRight();
        void moveDown();
        void moveLeft();
        Cursor();
};
