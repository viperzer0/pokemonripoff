#pragma once
#include <string>
//May not need this?
enum class Status {NONE,
    DISABLE, //Statuses that prevent fighter from moving
    POISON   //Statuses that deal damage to the fighter each turn
};

enum class Effects {NONE,MOVE_FAILED,RECOVERED,STATUS_WORSENS,TAKE_DAMAGE}; //All posible conditions of effects for various purposes. Different statuses may use different effects, and some may overlap. Use one from here if at all possible -- the distinction is mostly internal.

class StatusBase{
    protected:
        Status type;
        std::string effectHandle;
        std::string enterStatus;
        std::string continueStatus;
        std::string exitStatus;
        Effects curEffect;
    public:
        StatusBase(){type=Status::NONE;}
        virtual ~StatusBase(){};
        std::string getEnterString(){return enterStatus;}        
        Status getType(){return type;}
        std::string getEffectHandle(){return effectHandle;}

        void setEnterString(std::string input){enterStatus=input;}
        void setContinueString(std::string input){continueStatus=input;}
        void setExitString(std::string input){exitStatus=input;}
        void setEffectHandle(std::string input){effectHandle=input;}

        virtual std::string getEffectString(){
            //Only NONE effect applies to the parent function.
            return ""; 
        }
        
        virtual Effects doEffect(){
            curEffect=Effects::NONE;
            return Effects::NONE;
        }
};
