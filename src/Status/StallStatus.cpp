#include "StallStatus.h"

int StallStatus::turnsLeft = 0; //Initialize static variable

std::string StallStatus::getEffectString(){
    switch(curEffect){
        case Effects::MOVE_FAILED:
            return continueStatus;
            break;
        case Effects::RECOVERED:
            return exitStatus;
            break;
        default:
            return "";
            break;
    }
}

Effects StallStatus::doEffect(){//Only SLIGHTLY different then DisableStatus. We may not need this function at all! Lemme try it
    if(turnsLeft>0){
        curEffect=Effects::MOVE_FAILED;
        turnsLeft--;
        return Effects::MOVE_FAILED;
    }
    else{
        curEffect=Effects::RECOVERED;
        return Effects::RECOVERED;
    }
}



        
