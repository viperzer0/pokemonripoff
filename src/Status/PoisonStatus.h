#pragma once

#include "StatusEffects.h"

class PoisonStatus: public StatusBase{
    private:
        int exitChance;
    public:
        PoisonStatus(){
            type=Status::POISON;
            effectHandle="PSN";
            exitChance=20;
            enterStatus = " falls ill!";
            continueStatus = " takes damage!";
            exitStatus = " recovers from its sickness!";
        };

        std::string getEffectString();

        Effects doEffect();
};


