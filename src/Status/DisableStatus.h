#pragma once

#include "StatusEffects.h"

class DisableStatus: public StatusBase{
    public:
        DisableStatus() {
            type=Status::DISABLE;
            effectHandle="DSBL";
            enterStatus = " becomes disabled!";
            continueStatus = " can't move!";
            exitStatus = " is no longer disabled!";
        }

        std::string getEffectString();

        Effects doEffect();
};

