#pragma once

#include "StatusEffects.h"

class StallStatus: public StatusBase{
    private:
        std::string playerContStatus; //I think we can avoid this.
        static int turnsLeft; //Turns left tell end let's TRY THIS.
    public:
        StallStatus(){
            type=Status::DISABLE;
            //if turnsLeft isn't already declared (i.e by another class)
            if(turnsLeft!=1&&
               turnsLeft!=3&&
               turnsLeft!=5){
            turnsLeft=((rand()%3*2+1));//I think this always needs to be odd.
            }
            effectHandle="STL";
            enterStatus = " has been stalled!";
            continueStatus = " doesn't move!";
            playerContStatus  = "doesn't move!";
            exitStatus = "is no longer stalled!";
        }

        std::string getEffectString();

        Effects doEffect();
};


