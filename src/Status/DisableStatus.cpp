#include "DisableStatus.h"
std::string DisableStatus::getEffectString(){
    switch(curEffect){
        case Effects::MOVE_FAILED:
            return continueStatus;
            break;
        case Effects::RECOVERED:
            return exitStatus;
            break;
        default:
            return "";
            break;
    }
}

Effects DisableStatus::doEffect(){
    if(rand()%100<25){
        curEffect=Effects::MOVE_FAILED;
        return Effects::MOVE_FAILED; //MOVE FAILED
    }
    else if(rand()%100<75){
        curEffect=Effects::RECOVERED;
        return Effects::RECOVERED;
    }
    else{
        curEffect=Effects::NONE;
        return Effects::NONE;//Move succeeded
    }
}


