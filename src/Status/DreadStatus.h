#pragma once

#include "StatusEffects.h"
#include "DisableStatus.h"
//NOTE: DreadStatus is no longer inherited from DisableStatus.
//This will probably make it easier to keep track of actual statuses.
//It'll be for the best I think.
//All statuses will inherit from StatusBase
//
class DreadStatus: public StatusBase{
    private:
        std::string worsenStatus;
        int moveFailChance;
        int escapeChance;
        int worsenChance; 
    public:
        DreadStatus(){
            type=Status::DISABLE; //May not be needed?
            effectHandle="DRD";
            moveFailChance=25; //Also may not be needed
            worsenChance=15;
            escapeChance=20;
            enterStatus = " is filled with dread!";
            continueStatus = " is too horrified to move!";
            worsenStatus = " 's dread worsens!";
            exitStatus = " recovers from its initial shock!";
        }

        void setWorsenString(std::string input){worsenStatus=input;}        
        virtual std::string getEffectString(); 

        virtual Effects doEffect();
};
