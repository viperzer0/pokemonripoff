#include "PoisonStatus.h"

std::string PoisonStatus::getEffectString(){
    switch(curEffect){
        case Effects::TAKE_DAMAGE:
            return continueStatus;
            break;
        case Effects::RECOVERED:
            return exitStatus;
            break;
        default:
            return "";
            break;
    }
}

Effects PoisonStatus::doEffect(){
    if(rand()%100<exitChance){
        curEffect=Effects::RECOVERED;
        return Effects::RECOVERED;
    }
    else{
        curEffect=Effects::TAKE_DAMAGE;
        return Effects::TAKE_DAMAGE;
    }
}


