#include "DreadStatus.h"

std::string DreadStatus::getEffectString(){
    switch(curEffect){
        case Effects::MOVE_FAILED:
            return continueStatus;
            break; 
        case Effects::RECOVERED:
            return exitStatus;
            break;
        case Effects::STATUS_WORSENS:
            return worsenStatus;
            break;
        default:
            return "";
            break;
    }
}

Effects DreadStatus::doEffect(){
    int chance = rand() % 100;
    if(chance<=moveFailChance){
        curEffect=Effects::MOVE_FAILED;
        return Effects::MOVE_FAILED; //MOVE FAILED
    }
    else if (chance<=moveFailChance+escapeChance){
        curEffect=Effects::RECOVERED;
        return Effects::RECOVERED;//Dread removed
    } 

    else if (chance<=moveFailChance+escapeChance+worsenChance){
        if(worsenChance>0){
            worsenChance-=5;
            escapeChance+=5;
            moveFailChance+=10;
        }
        curEffect=Effects::STATUS_WORSENS; 
        //return Effects::STATUS_WORSENS;
        //Fighter should theoretically interpet this as a MOVE_FAILED
        //effect, so let's try returning that
        return Effects::MOVE_FAILED;
    }
    else{
        curEffect = Effects::NONE;
        return Effects::NONE;
    }
}

