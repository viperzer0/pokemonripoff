#include "Game.h"
#include "flags.h"
#include "fighterFromString.h"
#include "Fighter/DummyFighter.h"

Game::Game(int flags,std::string playerStr, std::string enemyStr) {

    running=true; 
    state=GameStates::Title;
    window.create(sf::VideoMode(640,480), "PokemonRipOff");
    font.loadFromFile("res/square.ttf");
    titleFont.loadFromFile("res/acknowtt.ttf");
    texture.loadFromFile("res/spritesheet.png");
    titleScreenTex.loadFromFile("res/title_screen.png");
    titleScreen.setTexture(titleScreenTex);
    
    //Load player
    player = fighterFromString(playerStr,1);
    //handleCMDInput should take care of misspelled player names. A NULL fighter should never be loaded, BUT if we DO for some reason get to this point.
    if(player == NULL){
        std::cout << "ERROR: Player fighter could not be loaded." << std::endl;
        running=false;
        return;
    }
    player->getSprite()->setTexture(texture);
    player->getSprite()->setDest(sf::IntRect(0,180,300,300));
    if(!(flags & Flags::NOSOUND)){//If NOSOUND flag is NOT setj
        playerCry.setBuffer(*player->getCry());//IDEA: Make a function that will handle testing flags within it so I don't have to add this for every single sound in the game.
    }

    //Load enemy
    enemy = fighterFromString(enemyStr,0);
    if(enemy==NULL){
        std::cout << "ERROR: Enemy fighter could not be loaded." << std::endl;
        running=false;
        return;
    }
    enemy->getSprite()->setTexture(texture);
    enemy->getSprite()->setDest(sf::IntRect(330,21,200,200));
    if(!(flags & Flags::NOSOUND)){
        enemyCry.setBuffer(*enemy->getCry());
    }

    //Load texts
    move0.setFont(font);
    move1.setFont(font); move2.setFont(font);
    move3.setFont(font);
    playerHealth.setFont(font);
    enemyHealth.setFont(font);
    status.setFont(font);
    playerEffect.setFont(font);
    enemyEffect.setFont(font);
    pressEnter.setFont(titleFont);
    
    move0.setCharacterSize(30);
    move1.setCharacterSize(30);
    move2.setCharacterSize(30);
    move3.setCharacterSize(30);
    playerHealth.setCharacterSize(30);
    enemyHealth.setCharacterSize(45);
    status.setCharacterSize(30);
    playerEffect.setCharacterSize(20);
    enemyEffect.setCharacterSize(20);
    pressEnter.setCharacterSize(33);

    enemyHealth.setPosition(24,24);
    playerHealth.setPosition(13,428);
    move0.setPosition(130,355);
    move1.setPosition(400,355);
    move2.setPosition(130,425);
    move3.setPosition(400,425);
    status.setPosition(130,320);
    enemyEffect.setPosition(24,76);
    playerEffect.setPosition(13,416);
    pressEnter.setPosition(145, 391); 

    pressEnter.setFillColor(sf::Color::Black);
    moves.setMove(0,player->getMoveName(0));
    moves.setMove(1,player->getMoveName(1));
    moves.setMove(2,player->getMoveName(2));
    moves.setMove(3,player->getMoveName(3));

    status.setString("A wild " + enemy->getName() + " approaches!");
    pressEnter.setString("PRESS ENTER TO BEGIN");
        
    //Load music! 
    if(!(flags & Flags::NOMUSIC)){//If the NOMUSIC flag is NOT set...
        background.openFromFile("res/background_music.wav");
        background.setVolume(50.0f);
        background.play();
        background.setLoop(true);
    }
}
