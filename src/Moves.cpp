#include "Moves.h"

void Moves::setMove(int index, std::string move){
    switch(index){
        case 0:
            move0 = move;
            break;
        case 1:
            move1 = move;
            break;
        case 2:
            move2 = move;
            break;
        case 3:
            move3 = move;
            break;
    }
}

std::string Moves::getMove(int index){
    switch(index){
        case 0:
            return move0;
            break;
        case 1:
            return move1;
            break;
        case 2:
            return move2;
            break;
        case 3:
            return move3;
            break;
    }
    return "";
}

void Moves::cursorMove(Directions direction){
    cursor.moveDir(direction);
}

//Okay, the more I think about it, the more I think that this may not be the
//right way to go about it. Or maybe it will be. I'm thinking that modifying the
//strings that appear on the screen may not be the best way to go about this.
//BUT I think I can still get the unmodified strings from the fighter objects so
//I can be like "FIGHTER uses MOVE!"
void Moves::updateText(){
    switch(cursor.getPosition()){

        case 0:
            if(move0[0]!='>'){
                move0.insert(0,1,'>');
            }
            if(move1[0]=='>'){
                move1.erase(0,1);
            }
            if(move2[0]=='>'){
                move2.erase(0,1);
            }
            if(move3[0]=='>'){
                move3.erase(0,1);
            }
            break;
        case 1:
            if(move0[0]=='>')
                move0.erase(0,1);
            if(move1[0]!='>')
                move1.insert(0,1,'>');
            if(move2[0]=='>')
                move2.erase(0,1);
            if(move3[0]=='>')
                move3.erase(0,1);
            break;
        case 2:
            if(move0[0]=='>')
                move0.erase(0,1);
            if(move1[0]=='>')
                move1.erase(0,1);
            if(move2[0]!='>')
                move2.insert(0,1,'>');
            if(move3[0]=='>')
                move3.erase(0,1);
            break;

        case 3:
            if(move0[0]=='>')
                move0.erase(0,1);
            if(move1[0]=='>')
                move1.erase(0,1);
            if(move2[0]=='>')
                move2.erase(0,1);
            if(move3[0]!='>')
                move3.insert(0,1,'>');
            break;
    }
}

int Moves::getCursorPosition(){
    return cursor.getPosition();
}

