#pragma once

#include "Fighter.h"

class KTFighter: public Fighter{
    public:
        KTFighter(bool player) : Fighter(player){
            sprite = new AnimateSprite();
            addRect(sf::IntRect(0,900,200,200));
            addRect(sf::IntRect(200,900,300,300));
            addRect(sf::IntRect(500,900,300,300));
            addRect(sf::IntRect(200,900,300,300));
            addRect(sf::IntRect(500,900,300,300));
            if(isPlayer()) addRectToAnimate(1);
            else addRectToAnimate(0);
            sprite->setFrame(0);
            sprite->updateSprite();

            setCry("res/cries/oof.wav");
            name = "KT";
            move0= "Floor";
            move1= "Bamboozle";
            move2= "Potato";
            move3= "Flout";
            
            maxHealth=100;
            curHealth=maxHealth;
            defenseRatio=1;
        }

        void action0(Fighter *enemy);
        void action1(Fighter *enemy);
        void action2(Fighter *enemy);
        void action3(Fighter *enemy);
};
