#include "JoshFighter.h"
#include "../Status/DisableStatus.h"
#include <iostream>

void JoshFighter::action0(Fighter *enemy){
    defaultAnimation();
    if(this->status->getType()!=Status::NONE){
        delete status;
        status = new StatusBase();
        addMsg(this->getName() + " cleared all status effets!");
    }
    else{
        addMsg("But nothing happens!");
    }
}

void JoshFighter::action1(Fighter *enemy){
    defaultAnimation();
    int amountStolen = enemy->getCurHealth()*((rand()%10+1.0)/100.0); //between 1 and 10% of enemy's current health.
    int actualAmount = enemy->takeDamage(amountStolen);
    std::cout << amountStolen << "\t" << actualAmount << std::endl;
    this->healDamage(actualAmount);
    addMsg(this->getName() + " sucks  " + std::to_string(actualAmount) + " HP from the opponent!");
}

void JoshFighter::action2(Fighter *enemy){
    defaultAnimation();
    if(rand()%100<50){
        StatusBase* status = new DisableStatus();
        enemy->inflictStatus(status);
        addMsg(enemy->getName() + status->getEnterString());
    }
    else{
        addMsg("But nothing happens!");
    }
}

void JoshFighter::action3(Fighter *enemy){
    defaultAnimation();
    enemy->takeDamage(20);
}







