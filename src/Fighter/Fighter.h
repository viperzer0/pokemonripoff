#pragma once

#include <string>
#include <queue>

#include "../Status/StatusEffects.h"
#include "../AnimateSprite.h"
#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/Rect.hpp>
#include <SFML/Graphics/Texture.hpp>
#include <SFML/Audio/SoundBuffer.hpp>

//Base Class for Fighters
class Fighter{
    protected:
        AnimateSprite *sprite;
        std::vector<sf::IntRect> srcs;
        
        bool player;

        //String Stuff
        std::string name;
        std::string move0;
        std::string move1;
        std::string move2;
        std::string move3;
        
        std::queue<std::string> msgQueue;
        //Stat Things;
        int curHealth;
        int maxHealth;
        float defenseRatio;
        float evadeRatio;
        StatusBase *status;//Pointer to any number of weird statusi.

        sf::SoundBuffer *cry; //Simliar to texture, not actually played, leave up to Game to actually make sounds I guess?

    public:
        //Constructor stuff
        virtual void action0(Fighter *enemy) = 0;
        virtual void action1(Fighter *enemy) = 0;
        virtual void action2(Fighter *enemy) = 0;
        virtual void action3(Fighter *enemy) = 0;
        
        Fighter(bool isPlayer){
            setIsPlayer(isPlayer);
            status = new StatusBase();//Not really gonna do anything now but hey it's nice to know.
        }

        virtual ~Fighter(){//Delete pointers when fighter is destroyed
            delete sprite;
            delete status;
            delete cry;
        }
        //Getters
        AnimateSprite *getSprite();
        std::string getName();
        std::string getMoveName(int index);
        std::string getMsg();
        StatusBase *getStatus();
        sf::SoundBuffer* getCry();
        bool queueEmpty();

        int getCurHealth();
        int getMaxHealth();
        bool isDead();
        bool isPlayer(){return player;};

        //Setters
        void resetCurStat();        
        void addMsg(std::string);
        void setCry(std::string location);
        void addRect(sf::IntRect src);
        void addRectToAnimate(int index);
        void setIsPlayer(bool isPlayer){this->player=isPlayer;}
        
        //Combat Stuff
        int takeDamage(int amount);
        void healDamage(int amount);
        void inflictStatus(StatusBase *status);
        void incDefense();
        void decDefense(); 
        void move(Fighter *enemy, int action);
        void defaultAnimation();
};
