#include "EliFighter.h"
#include "../Status/DreadStatus.h"
#include "../Status/DisableStatus.h"

/* Name: Eli
 * Health: 100
 * Move: Screech
 * Description: A terrible screech that fills the enemy's heart with dread!
 * Move: The Game
 * Description: A frustrating flurry that irritates an opponent!
 * Move: Labyrinth
 * Description: Confounds the opponent with the power of the labyrinth at the
 * expense of the fighter's own health!
 * Move: Amaze
 * Description: The fighter calls upon the healing power of the maze!
 */
void EliFighter::action0(Fighter *enemy){
    defaultAnimation();
    enemy->takeDamage(15);
    if(rand()%100<=20){
        StatusBase *newStat = new DreadStatus();
        addMsg(enemy->getName() + newStat->getEnterString());
        enemy->inflictStatus(newStat);
    }
}

void EliFighter::action1(Fighter *enemy){
    defaultAnimation();
    enemy->takeDamage(25);
}

void EliFighter::action2(Fighter *enemy){
    defaultAnimation();
    if(rand()%100<=80){
        StatusBase *newStat = new DisableStatus();
        addMsg(enemy->getName() + newStat->getEnterString());
        addMsg(this->getName() + " hurts itself in the process!");
        enemy->inflictStatus(newStat);
        this->takeDamage(10);
    }
    else{
        addMsg("...but nothing happens!");
    }
}

void EliFighter::action3(Fighter *enemy){
    defaultAnimation();
    addMsg(this->getName() + " heals its wounds!");
    this->healDamage(15);
}

