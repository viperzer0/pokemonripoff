#pragma once

#include "Fighter.h"

class DominicFighter: public Fighter{
    public:
        DominicFighter(bool player) : Fighter(player){
            sprite = new AnimatedSprite();
            addRect(sf::IntRect(0,1500,200,200));
            addRect(sf::IntRect(200,1500,300,300));
            addRect(sf::IntRect(500,1500,300,300));
            if(isPlayer()) addRectToAnimate(1);
            else addRectToAnimate(0);
            sprite->setFrame(0);
            sprite->updateSprite();
            setCry("res/cries/oof.wav");

            name = "Dominic";
            move0= "Obscure Reference!";
            move1= "Criticize";
            move2= "E";
            move3= "e":

            maxHealth=100;
            curHealth=maxHealth;
            defenseRatio=1;
        }

        void action0(Fighter *enemy);
        void action1(Fighter *enemy);
        void action2(Fighter *enemy);
        void action3(Fighter *enemy);
};

