#pragma once

#include "Fighter.h"

class JoshFighter: public Fighter{
    public:
        JoshFighter(bool player) : Fighter(player){
            sprite = new AnimateSprite();
            addRect(sf::IntRect(0,600,200,200));
            addRect(sf::IntRect(200,600,300,300));
            addRect(sf::IntRect(500,600,300,300));
            addRect(sf::IntRect(200,600,300,300));
            addRect(sf::IntRect(500,600,300,300));
            if(isPlayer()) addRectToAnimate(1);
            else addRectToAnimate(0);
            sprite->setFrame(0);
            sprite->updateSprite();

            setCry("res/cries/oof.wav");

            name = "Josh";
            move0 = "Logic";
            move1 = "Magic";
            move2 = "Horse";
            move3 = "Oof";

            maxHealth = 100;
            curHealth=maxHealth;
            defenseRatio=1;
        }

        void action0(Fighter *enemy);
        void action1(Fighter *enemy);
        void action2(Fighter *enemy);
        void action3(Fighter *enemy);
};

    
