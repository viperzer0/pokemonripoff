#pragma once

#include "Fighter.h"

class GinniFighter: public Fighter{
    private:
        int timesUsedSushiRoll;

    public:
        GinniFighter(bool player): Fighter(player){
            sprite = new AnimateSprite();
            addRect(sf::IntRect(0,1200,200,200));
            addRect(sf::IntRect(200,1200,300,300));
            addRect(sf::IntRect(500,1200,300,300));
            addRect(sf::IntRect(800,1200,300,300));
            if(isPlayer()) addRectToAnimate(1);
            else addRectToAnimate(0);
            sprite->setFrame(0);
            sprite->updateSprite();
            setCry("res/cries/oof.wav");

            name = "Ginni";
            move0= "Huh?";
            move1= "Sushi Roll";
            move2= "Pie";
            move3= "Hair Whip";

            maxHealth=100;
            curHealth=maxHealth;
            defenseRatio=1;
            timesUsedSushiRoll=0;
        }

        void action0(Fighter *enemy);
        void action1(Fighter *enemy);
        void action2(Fighter *enemy);
        void action3(Fighter *enemy);

        void squatAnimation();
        void spinAnimation();
};
