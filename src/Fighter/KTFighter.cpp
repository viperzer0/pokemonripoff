#include "KTFighter.h"
#include "../Status/DisableStatus.h"

void KTFighter::action0(Fighter *enemy){
    defaultAnimation();
    if(rand()%100<=55){
        StatusBase *newStat = new DisableStatus();
        newStat->setEnterString(" starts giggling!");
        newStat->setContinueString(" can't stop giggling!");
        newStat->setExitString(" finally gets ahold of itself");
        addMsg(enemy->getName() + newStat->getEnterString());
        enemy->inflictStatus(newStat);
    }
    else{
        addMsg("...But nothing happens.");
    }
}

void KTFighter::action1(Fighter *enemy){
    defaultAnimation();
    addMsg(getName() + " bamboozles her opponent!");
    enemy->takeDamage(rand()%20+10);
}

void KTFighter::action2(Fighter *enemy){
    defaultAnimation();
    int amount = rand()%10+10;
    addMsg(getName() + " regains" + std::to_string(amount) + " HP!");
    healDamage(amount);
    incDefense();
}

void KTFighter::action3(Fighter *enemy){
    defaultAnimation();
    int chance = rand() % 100;
    if(chance<=25){
        addMsg(getName() + " plays a shrill note!");
        enemy->takeDamage(15);
    }
    else if(chance<=50){
        addMsg(getName() + " plays a soothing melody!");
        StatusBase *newStat = new DisableStatus();
        newStat->setEnterString(" is lulled to sleep!");
        newStat->setContinueString(" slumbers on!");
        newStat->setExitString(" wakes up!");
        addMsg(enemy->getName() + newStat->getEnterString());
        enemy->inflictStatus(newStat);
    }
    else if(chance<=75){
        addMsg(getName() + " beats the opponent with her flute!");
        enemy->takeDamage(25);
    }
    else{
        addMsg(getName() + " tootles her flootle!");
    }
}
