#include "GinniFighter.h"
#include "../Status/StallStatus.h"
#include "../Status/PoisonStatus.h"

void GinniFighter::action0(Fighter *enemy){
    squatAnimation();
    timesUsedSushiRoll=0;
    if(rand()%100<=75){
        StatusBase *newStat = new StallStatus();
        newStat->setEnterString(" confuses the opponent!");
        newStat->setContinueString(" says huh? instead");
        newStat->setExitString(""); // Let's try this
        addMsg(this->getName() + newStat->getEnterString());
        enemy->inflictStatus(newStat);

        StatusBase *playerStat = new StallStatus();
        playerStat->setEnterString("");
        playerStat->setContinueString(" says huh? instead");
        playerStat->setExitString("");
        this->inflictStatus(playerStat);
    }
    else{
        addMsg("...but nothing happens!");
    }
}
        
void GinniFighter::action1(Fighter *enemy){
    squatAnimation();
    //I kinda like using this-> because it looks very simliar to player->
    this->healDamage(10+5*timesUsedSushiRoll++); //Hey look at that magic!
    this->incDefense();
    addMsg(this->getName() + " gains " + std::to_string(10*timesUsedSushiRoll) + " HP!");
}

void GinniFighter::action2(Fighter *enemy){
    squatAnimation();
    timesUsedSushiRoll=0;
    addMsg(this->getName() + " baked " + enemy->getName() + " a pie!");
    if(rand()%100<33){
        enemy->healDamage(15);
        addMsg(enemy->getName() + " gains 15 HP!");
    }
    else{
        StatusBase *poison = new PoisonStatus();
        poison->setEnterString("");
        poison->setContinueString(" takes poison damage!");
        poison->setExitString(" is no longer poisoned");
        addMsg("But it was poisoned!!");
        enemy->inflictStatus(poison);
    }
}
                

void GinniFighter::action3(Fighter *enemy){
    spinAnimation();
    timesUsedSushiRoll=0;
    enemy->takeDamage(rand()%10+20);
}

void GinniFighter::squatAnimation(){
    sprite->resetRects();
    if(isPlayer()){
    addRectToAnimate(1);
    addRectToAnimate(2);
    addRectToAnimate(1);
    addRectToAnimate(2);
    addRectToAnimate(1);
    }
    else{
        addRectToAnimate(0);
    }
}

void GinniFighter::spinAnimation(){
    sprite->resetRects();
    if(isPlayer()){
    addRectToAnimate(1);
    addRectToAnimate(3);
    addRectToAnimate(1);
    addRectToAnimate(3);
    addRectToAnimate(1);
    }
    else{
        addRectToAnimate(0);
    }
}

