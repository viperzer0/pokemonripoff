#include "DummyFighter.h"
#include "../Status/DisableStatus.h"
#include "../Status/DreadStatus.h"

void DummyFighter::action0(Fighter *enemy){
    defaultAnimation();
    int chance = rand() % 100;
    if(chance<25){
        addMsg("It's not very effective...");
        enemy->takeDamage(10);
    }
    else if(chance>=25&&chance<75){
        addMsg("AAAA");
        addMsg("BBBB");
        enemy->takeDamage(30);
    }
    else if(chance>=75){
        addMsg("It's super effective!");
        enemy->takeDamage(50);
    }
}

void DummyFighter::action1(Fighter *enemy){
    defaultAnimation();
    incDefense();
}

void DummyFighter::action2(Fighter *enemy){
    defaultAnimation();
    StatusBase *newStat = new DisableStatus();
    addMsg(enemy->getName() + newStat->getEnterString());
    enemy->inflictStatus(newStat);
}

void DummyFighter::action3(Fighter *enemy){
    defaultAnimation();
    StatusBase *newStat = new DreadStatus();
    addMsg(enemy->getName() + newStat->getEnterString());
    enemy->inflictStatus(newStat);
    enemy->takeDamage(10);
}


