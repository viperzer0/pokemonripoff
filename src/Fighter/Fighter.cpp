#include "Fighter.h"

AnimateSprite *Fighter::getSprite(){
    return sprite; 
}
int Fighter::takeDamage(int amount){
    if(evadeRatio*100>rand()%100){
        int damageDealt = amount / defenseRatio;
        if(damageDealt>=curHealth){
            curHealth=0;
        }
        else{
            curHealth-=damageDealt;
        }
        return damageDealt;
    }
    else{
        addMsg(this->getName() + " avoids the attack!");
        return 0;
    }
} 
void Fighter::healDamage(int amount){
    if(curHealth+amount>=maxHealth){
        curHealth=maxHealth;
    }
    else{
        curHealth+=amount;
    }
}

void Fighter::inflictStatus(StatusBase *status){
    delete this->status;
    this->status = status;
}

std::string Fighter::getMoveName(int index){
    switch(index){
        case 0:
            return move0;
            break;
        case 1:
            return move1;
            break;
        case 2: return move2;
            break;
        case 3:
            return move3;
            break;
        default:
            return "NIL";
            break;
    }
}

int Fighter::getCurHealth(){
    return curHealth;
}

int Fighter::getMaxHealth(){
    return maxHealth;
}

std::string Fighter::getName(){
    return name;
}

StatusBase *Fighter::getStatus(){
    return status;
}

bool Fighter::isDead(){
    return (curHealth <= 0);
}

void Fighter::addMsg(std::string input){
    msgQueue.push(input);
}

std::string Fighter::getMsg(){
    std::string temp = msgQueue.front();
    msgQueue.pop(); //NOW we delete the first string in the queue. This is stupid.
    return temp;
}


bool Fighter::queueEmpty(){
    return msgQueue.empty();
}

void Fighter::incDefense(){
    if(defenseRatio>=2.0){
        addMsg(getName() + " can't raise its defense any higher!");
    }
    else{
        defenseRatio+=0.1;
        addMsg(getName() + " raises its defense!");
    }
}

void Fighter::decDefense(){
    if(defenseRatio<=0.5){
        addMsg(getName() + "'s defense won't go lower!");
    }
    else{
        defenseRatio-=0.1;
        addMsg(getName() + "'s defenses are lowered!");
    }
}

void Fighter::move(Fighter* enemy,int action){
    bool doAction=true;
    Status type = status->getType();
    Effects effect = status->doEffect();
    std::string resultMessage = status->getEffectString();

    if(effect==Effects::MOVE_FAILED){ //Might not need status worsens? Removed || Effects::STATUS_WORSENS for now.
        doAction=false;
    }

    if(effect==Effects::TAKE_DAMAGE){
        takeDamage(5); //Might rewrite so that we can bypass defense value. 
    }
    
    if(effect==Effects::RECOVERED){
        delete status;
        status = new StatusBase();
    }
    if(resultMessage!=""){
        addMsg(getName() + resultMessage);
    }
    
    if(doAction){ 
        switch(action){
            case 0:
                action0(enemy);
                break;
            case 1:
                action1(enemy);
                break;
            case 2:
                action2(enemy);
                break;
            case 3:
                action3(enemy);
                break;
        }
    }
}

void Fighter::setCry(std::string location){
    cry = new sf::SoundBuffer;
    cry->loadFromFile(location);
}

sf::SoundBuffer* Fighter::getCry(){
    return cry;
}

void Fighter::addRect(sf::IntRect src){
    srcs.emplace_back(src.left, src.top, src.width, src.height);
}

void Fighter::addRectToAnimate(int index){
    sprite->addRect(srcs[index]);
}

void Fighter::defaultAnimation(){
    sprite->resetRects();
    if(isPlayer()){
        for(int i=1;i<srcs.size();i++){
            addRectToAnimate(i);
        }
    }
    else{
        addRectToAnimate(0);
    }
    sprite->setFrame(0);
    sprite->updateSprite();
}




