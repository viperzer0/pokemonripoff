#pragma once

#include "Fighter.h"

class PolarFighter: public Fighter{
    public:
        PolarFighter(bool player) : Fighter(player){
            sprite = new AnimateSprite();
            addRect(sf::IntRect(0,300,200,200));
            addRect(sf::IntRect(200,300,300,300));
            addRect(sf::IntRect(500,300,300,300));
            addRect(sf::IntRect(200,300,300,300));
            addRect(sf::IntRect(500,300,300,300));
            if(isPlayer()) addRectToAnimate(1);
            else addRectToAnimate(0);
            sprite->setFrame(0);
            sprite->updateSprite();
            setCry("res/cries/oof.wav");

            name = "Polar";
            move0= "Minatory Cronch";
            move1= "Balisong Whip";
            move2= "Endless Void";
            move3= "Party Streamers";

            maxHealth=100;
            curHealth=maxHealth;
            defenseRatio=1;
        }

        void action0(Fighter *enemy);
        void action1(Fighter *enemy);
        void action2(Fighter *enemy);
        void action3(Fighter *enemy);
};
