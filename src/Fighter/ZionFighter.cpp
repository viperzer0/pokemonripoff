#include "ZionFighter.h"
#include "../Status/DisableStatus.h"
#include "../Status/DreadStatus.h"

void ZionFighter::action0(Fighter *enemy){
    defaultAnimation();
    addMsg("Zion screams into the void.");
}

void ZionFighter::action1(Fighter *enemy){
    addMsg(this->getName() + " gets small!");
    if(this->evadeRatio<0.7){
        addMsg(this->getName() + " increases his evasion!");
        evadeRatio += 0.1;
    }
    else{
        addMsg("But his evasion can't get any higher!");
    }
}

void ZionFighter::action2(Fighter *enemy){
    addMsg(this->getName() + " fires his rockets!");
    switch(rocketLevel){
        case 0:
            enemy->takeDamage(3+rand()%6);
            rocketLevel=0;
            break;
        case 1:
            enemy->takeDamage(7+rand()%9);
            rocketLevel=0;
            break;
        case 2:
            enemy->takeDamage(20+rand()%11);
            rocketLevel=0;
            break;
        case 3:
            enemy->takeDamage(40+rand()%21);
            rocketLevel=0;
            break;
        case 4:
            enemy->takeDamage(50+rand()%61);
            rocketLevel=0;
            break;
    }
}

void ZionFighter::action3(Fighter *enemy){
    if(rocketLevel>=4){
        addMsg("But his rockets won't upgrade any farther!");
    }
    else{
        rocketLevel += 1;
    }
}


