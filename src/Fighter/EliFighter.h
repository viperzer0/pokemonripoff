#pragma once

#include "Fighter.h"

class EliFighter: public Fighter{
    public:
        EliFighter(bool player): Fighter(player){
            sprite = new AnimateSprite();
            addRect(sf::IntRect(0,0,200,200));
            addRect(sf::IntRect(200,0,300,300));
            addRect(sf::IntRect(500,0,300,300));
            addRect(sf::IntRect(200,0,300,300));
            addRect(sf::IntRect(500,0,300,300));
            if(isPlayer()) addRectToAnimate(1);
            else addRectToAnimate(0);
            sprite->setFrame(0);
            sprite->updateSprite();
            setCry("res/cries/amp_slow_oof.wav");

            name = "Eli";
            move0= "Screech";
            move1= "The Game";
            move2= "Labyrinth";
            move3= "Amaze";

            maxHealth=100;
            curHealth=maxHealth;
            defenseRatio=1;
        }
        
        void action0(Fighter *enemy);
        void action1(Fighter *enemy);
        void action2(Fighter *enemy);
        void action3(Fighter *enemy);
};



