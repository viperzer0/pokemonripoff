#pragma once
#include "Fighter.h"

class ZionFighter: public Fighter{
    private:
        int rocketLevel=0;

    public:
        ZionFighter(bool player) : Fighter(player){
            //Things that are created now:
            //new AnimateSprite()
            //Add source rectangles for spritesheet
            //setFrame to 0.
            //Frame 0 will be 200x200 face texture
            
            sprite = new AnimateSprite();
            addRect(sf::IntRect(0,1200,200,200));//Face texture
            addRect(sf::IntRect(200,0,300,300));//Back textures
            addRect(sf::IntRect(500,0,300,300));
            addRect(sf::IntRect(200,300,300,300));
            addRect(sf::IntRect(500,300,300,300));
            if(isPlayer()) addRectToAnimate(1);
            else addRectToAnimate(0);
            sprite->setFrame(0);
            sprite->updateSprite();

            setCry("res/cries/oof.wav");
            //Things that will be done later in gameInit
            //set sprite destination (depends on if fighter is player or enemy)
            //setFrame will likely be changed!
            //Actual texture will be set!
            //Entire sprite will be updated!
            
            name = "Zion";
            move0= "Drone Swarm";
            move1= "Evade";
            move2= "Rockets";
            move3= "Upgrade Rockets";

            maxHealth=100;
            curHealth=maxHealth;
            defenseRatio = 1;
            evadeRatio = 0;
        } //Inline initialization list kinda thing but ALSO set some variables to things

        void action0(Fighter *enemy);
        void action1(Fighter *enemy);
        void action2(Fighter *enemy);
        void action3(Fighter *enemy);
};


