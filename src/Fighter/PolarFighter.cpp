#include "PolarFighter.h"
#include "../Status/DreadStatus.h"
#include "../Status/DisableStatus.h"

/*
 * Name: Polar
 * Health: 100
 * Move: Minatory Cronch
 * Description: Pounds the opponent with a deadly minatory cronch
 * Move: Balisong Whip
 * Description: Intimidates and mutilates the opponent with a balisong whip.
 * Move: Endless Void
 * Description: Unleashes the power of the endless void on their opponent
 * Move: Party Streamers
 * Description: Baffles and befuddles the opponent with party streamers
 */
void PolarFighter::action0(Fighter *enemy){
    defaultAnimation();
    if(rand()%100<=10){
        addMsg("It's super effective!");
        enemy->takeDamage(100);
    }
    else{
        enemy->takeDamage(10);
    }
}

void PolarFighter::action1(Fighter *enemy){
    defaultAnimation();
    enemy->takeDamage(20);
}

void PolarFighter::action2(Fighter *enemy){
    defaultAnimation();
    if(rand()%100<=50){
        StatusBase *newStat = new DreadStatus();
        addMsg(enemy->getName() + newStat->getEnterString());
        enemy->inflictStatus(newStat);
        enemy->takeDamage(50);
    }
    else{
        addMsg(enemy->getName() + " is not intimidated.");//NOTE: Maybe should make a failString for this situation. Or do nothing at all!
    }
}

void PolarFighter::action3(Fighter *enemy){
    defaultAnimation();
    if(rand()%100<=50){
        StatusBase *newStat = new DisableStatus();
        addMsg(enemy->getName() + newStat->getEnterString());//Maybe should change this BACK to individual fighter strings.
        //We'll use both, how about that? Leave it up to the user to decide.
        enemy->inflictStatus(newStat);
    }
    else{
        addMsg("...but nothing happened.");
    }
}

