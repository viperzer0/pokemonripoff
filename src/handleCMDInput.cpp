#include "handleCMDInput.h"
#include "flags.h"
#include "config.h"
#include <vector>
#include <algorithm>
#include <cstdlib>
#include <cstring>
#include <iostream>

int handleCMDInput( std::string &output1, std::string &output2,int argc, char **argv){
    int flags=0; //Gonna return this 
    bool error=false;
    const std::vector<std::string> fighters = {"dummy","eli","polar","josh","kt","ginni","zion"};

    //I think the best thing to do would be to pop out all the -flag arguments
    //so we're left with the fighters and then mess with that.
    
    std::vector<std::string> arguments(argv+1,argv+argc);//string containing all arguments (minus filename?)
    
    std::vector<std::string>::const_iterator it;
    
    it = std::find(arguments.begin(),arguments.end(),"--nomusic");
    if(it!=arguments.end()){
        flags |= Flags::NOMUSIC;
        arguments.erase(it);
    }
    
    it = std::find(arguments.begin(),arguments.end(),"--nosound");
    if(it!=arguments.end()){
        flags |= Flags::NOSOUND;
        arguments.erase(it);
    }

    it = std::find(arguments.begin(),arguments.end(),"--noaudio");
    if(it!=arguments.end()){
        flags |= Flags::NOMUSIC;
        flags |= Flags::NOSOUND;
        arguments.erase(it);
    }
    it = std::find(arguments.begin(),arguments.end(),"-h");
    if(it!=arguments.end()){
        error=true;
    }

    if(arguments.size()==0){
        output1=fighters.at(rand()%(fighters.size()-1));
        output2=fighters.at(rand()%(fighters.size()-1));
    }
    else if(arguments.size()==2){
        std::string player = arguments[arguments.size()-2];
        std::string enemy = arguments[arguments.size()-1];

        if(player!="-"){
            it = std::find(fighters.begin(),fighters.end(),player);
            if(it!=fighters.end())
                output1 = *it;
            else{
                std::cerr << "Error loading player fighter" << std::endl;
                error=true;
            }
        }
        else{
            output1=fighters.at(rand()%(fighters.size()-1));
        }

        if(enemy!="-"){
            it = std::find(fighters.begin(),fighters.end(),enemy);
            if(it!=fighters.end())
                output2 = *it;
            else{
                std::cerr << "Error loading enemy fighter" << std::endl;
                error=true;
            }
        }
        else{
            output2=fighters.at(rand()%(fighters.size()-2));
        }
    }
    else{
        error=true;
    }

    if(error==true){
        std::cout << 
            "PokemonRipOff v." 
            << VERSION_MAJOR << "."
            << VERSION_MINOR << "\n"
            << "List of Fighters:\n";
        for(std::vector<std::string>::const_iterator it = fighters.begin();it!=fighters.end();++it){ 
            std::cout<<"\t"<< *it << std::endl;
        }
        std::cout<<"\t Enter \"-\" to insert a random fighter." << std::endl;
        std::cout<< "Run ./PokemonRipOff (OPTIONS) [FIGHTER] [ENEMY] and insert fighter names between brackets.\n" <<
            "Other options include:\n" <<
            "\t-h -- Prints this help menu\n" <<
            "\t--nomusic -- Runs without music\n" <<
            "\t--nosound -- Runs without sound effects\n" <<
            "\t--noaudio -- Runs without any music OR sound effects"
            << std::endl;
        flags = -1;
    }

    return flags; 



    /*REPLACING HOPEFULLY
    //Lowercase all inputs  
    for(int i=1;i<argc;++i){
        char *ptr = argv[i];
        for(;*ptr;++ptr) *ptr = tolower(*ptr);
    }

    if(argc==1){//One argument (filename) should randomly generate fighters.
        output1=fighters.at(rand()%(fighters.size()-1));
        output2=fighters.at(rand()%(fighters.size()-1));
        return 0;
    }
    else if(argc==3){//Three arguments, filename, PLAYER, ENEMY
        std::vector<std::string>::const_iterator location;
        location = find(fighters.begin(),fighters.end(),argv[1]);
        if(location!=fighters.end())
            output1 = *location;
        else{
            std::cerr << "Error loading player fighter" << std::endl;
            return -1;
        }
        

        location = find(fighters.begin(),fighters.end(),argv[2]);
        if(location!=fighters.end())
            output2 = *location;
        else{
            std::cerr << "Error loading enemy fighter" << std::endl;
            return -1;
        }
        return 0;
    }
    else{
        std::cout << 
            "PokemonRipOff v." 
            << VERSION_MAJOR << "."
            << VERSION_MINOR << "\n"
            << "List of Fighters:\n";
        for(std::vector<std::string>::const_iterator it = fighters.begin();it!=fighters.end();++it){ 
            std::cout<<"\t"<< *it << std::endl;
        }
        std::cout<< "Run ./PokemonRipOff [FIGHTER] [ENEMY] and insert fighter names between brackets.\n"
            << std::endl;
        return -1;
    }
    */
}
    


