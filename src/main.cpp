#include "Game.h"
#include "handleCMDInput.h"
#include <iostream>
#include <ctime>

int main(int argc, char **argv){
    srand(time(NULL));
    //Argument 0 is file name
    std::string player;
    std::string enemy;
    
    int flags=0;
    flags = handleCMDInput(player,enemy,argc,argv);
    if(flags==-1) return -1;

    Game game(flags, player, enemy);

    while(game.running){
        game.events();
        game.loop();
        game.render();
    }

    return 0;
}

