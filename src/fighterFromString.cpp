#include "fighterFromString.h"
#include "Fighter/EliFighter.h"
#include "Fighter/PolarFighter.h"
#include "Fighter/DummyFighter.h"
#include "Fighter/KTFighter.h"
#include "Fighter/GinniFighter.h"
#include "Fighter/JoshFighter.h"
#include "Fighter/ZionFighter.h"
Fighter *fighterFromString(std::string fighter, bool player){
    if(fighter=="dummy")
        return new DummyFighter(player);
    else if(fighter=="eli")
        return new EliFighter(player);
    else if(fighter=="polar")
        return new PolarFighter(player);
    else if(fighter=="kt")
        return new KTFighter(player);
    else if(fighter=="ginni")
        return new GinniFighter(player);
    else if(fighter=="josh")
        return new JoshFighter(player);
    else if(fighter=="zion")
        return new ZionFighter(player);
    else
        return NULL;
}

