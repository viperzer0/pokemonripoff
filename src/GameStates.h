#pragma once
/*
 * Title -- Title Screen w/out text
 * Title_Text -- Title Screen w/ Text (flashes back and forth between Title and Title_Text
 * P_Turn -- Player Turn
 * P_Wait -- Player Animtion
 * P_Result -- Display status message queue here
 * (Same for E_Turn, E_Wait, and E_Result but for enemy)
 * Player Win: Game ends here
 * Enemy Win: Game ends here
 */
enum class GameStates {Title,Title_Text,P_Turn,P_Wait,P_Result,E_Turn,E_Wait,E_Result,P_Win,E_Win};
