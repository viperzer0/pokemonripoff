#pragma once
#include <SFML/Graphics.hpp>
#include <SFML/Graphics/Font.hpp>
#include <SFML/Graphics/Text.hpp>
#include <SFML/Audio.hpp>

#include "Fighter/Fighter.h"
#include "Cursor.h"
#include "Moves.h"
#include "GameStates.h"

class Game{
    private:
        GameStates state;

        Moves moves; //This is a somewhat misleading name but it actually keeps track of the move names and which one has the cursor on it.
        sf::Clock statusClock;

        //Put resources here I guess?
        sf::RenderWindow window;
        sf::Font font;
        sf::Font titleFont;
        Fighter *player; 
        Fighter *enemy;

        sf::Music background;
        sf::Music titleMusic;
        
        sf::Sprite titleScreen;

        sf::Text move0;
        sf::Text move1;
        sf::Text move2;
        sf::Text move3;
        sf::Text playerHealth;
        sf::Text enemyHealth;
        sf::Text status;
        sf::Text playerEffect;
        sf::Text enemyEffect;
        sf::Text pressEnter;
        sf::Texture texture;
        sf::Texture titleScreenTex; 
        sf::Sound playerCry;
        sf::Sound enemyCry;

    public:
        bool running;

        Game(int flags, std::string playerStr, std::string enemyStr);

        void events();
 
        void loop();

        void render();

        ~Game();
};

