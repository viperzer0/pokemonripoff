#include "Game.h"

void Game::render(){
    switch(state){
        case GameStates::Title:
            window.clear(sf::Color::Black);
            window.draw(titleScreen);
            window.display();
            break;

        case GameStates::Title_Text:
            window.clear(sf::Color::Black);
            window.draw(titleScreen);
            window.draw(pressEnter);
            window.display();
            break;

        default:
            window.clear(sf::Color::Black);
            window.draw(player->getSprite()->getSprite());
            window.draw(move0);
            window.draw(move1);
            window.draw(move2);
            window.draw(move3);
            window.draw(status);
            window.draw(enemyHealth);
            window.draw(playerHealth);
            window.draw(enemyEffect);
            window.draw(playerEffect);
            window.draw(enemy->getSprite()->getSprite());
            window.display();
            break;

    }
}
