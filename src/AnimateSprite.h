#pragma once

#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/Texture.hpp>
#include <SFML/Graphics/Rect.hpp>

//Container for animated sprite frames
class AnimateSprite{
    private:
        sf::Sprite sprite;
        sf::Texture texture;
        std::vector<sf::IntRect> srcs;
        sf::IntRect dest;
        int currentSrc;

    public:
        void setTexture(sf::Texture &texture);
        void resetRects();
        void addRect(sf::IntRect src);
        void setDest(sf::IntRect dest);
        void updateSprite();        
        sf::Sprite getSprite();

        void nextFrame();
        void setFrame(int frameNum);
        int countFrames();
        int getFrame(); 
};
