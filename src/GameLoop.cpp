#include "Game.h"
#include <cstdlib>
#include <iostream>

void Game::loop(){
    //Process loop
    moves.updateText();

    move0.setString(moves.getMove(0));
    move1.setString(moves.getMove(1));
    move2.setString(moves.getMove(2));
    move3.setString(moves.getMove(3));

    switch(state){
        case GameStates::Title:
            if(statusClock.getElapsedTime()>sf::seconds(0.75)){
                state=GameStates::Title_Text;
                statusClock.restart();
                break;
            }
        case GameStates::Title_Text:
            if(statusClock.getElapsedTime()>sf::seconds(0.75)){
                state=GameStates::Title;
                statusClock.restart();
                break;
            }

        case GameStates::P_Turn:
            //Currently processing user events over in events(). Upon "Enter", P_Turn will
            //complete, P_Announce is gona be folded INTO that as part of the
            //event block. After a delay, then announce P_Move_Result.
            break;
        case GameStates::P_Wait://Animation and "Player used X" here
            {
                static int timesRepeat =0;
                if(statusClock.getElapsedTime()>sf::seconds(0.25)){
                    if(timesRepeat<player->getSprite()->countFrames()){
                        player->getSprite()->nextFrame();
                        player->getSprite()->updateSprite();
                        statusClock.restart();
                        timesRepeat++;
                        break;
                    }
                    else{
                        timesRepeat=0;

                        //Pull first status update off queue and put it there for the 2 second delay while P_result is updating
                        if(!(player->queueEmpty()))
                            status.setString(player->getMsg());

                        state=GameStates::P_Result;
                        statusClock.restart();
                        break;
                    }
                }
                
            }

        case GameStates::P_Result://Empty Message Queue here
            {
                if(!(player->queueEmpty())){
                    if(statusClock.getElapsedTime()>sf::seconds(2)){
                        status.setString(player->getMsg());
                        statusClock.restart();
                    }
                }
                else{
                    if(statusClock.getElapsedTime()>sf::seconds(2)){
                        status.setString("");
                        if(enemy->isDead())
                            state=GameStates::P_Win;
                        else
                            state=GameStates::E_Turn;
                        statusClock.restart();
                    }
                }
                break;
            }

        case GameStates::E_Turn:
            {
            enemyCry.play();
            int move = rand() % 4;
            enemy->move(player,move);
            status.setString(enemy->getName() + " uses " + enemy->getMoveName(move));
            state=GameStates::E_Wait;
            break;
            }
       
        case GameStates::E_Wait:
            {
            if(statusClock.getElapsedTime()>sf::seconds(2)){
                if(!(enemy->queueEmpty()))
                    status.setString(enemy->getMsg());//Again, pop first status update off queue for the 2 second delay in E_Result

                statusClock.restart();
                state = GameStates::E_Result;
            }
            break;
            }

        case GameStates::E_Result://Empty Message Queue here
            {
                if(!(enemy->queueEmpty())){
                    if(statusClock.getElapsedTime()>sf::seconds(2)){
                        status.setString(enemy->getMsg());
                        statusClock.restart();
                    }
                }
                else{
                    if(statusClock.getElapsedTime()>sf::seconds(2)){
                    status.setString("");
                        if(player->isDead())
                            state=GameStates::E_Win;
                        else
                            state=GameStates::P_Turn;

                        statusClock.restart();
                    }
                }
            break;
            }

        case GameStates::P_Win:
            {
            status.setString(enemy->getName() + " has fainted!");
            if(statusClock.getElapsedTime()>sf::seconds(2)){
                running=false;
            }
            break;
            }

        case GameStates::E_Win:
            {
            status.setString(player->getName() + " has fainted!");
            if(statusClock.getElapsedTime()>sf::seconds(2)){
                running=false;
            }
            break;
            }
    }
    enemyHealth.setString(std::to_string(enemy->getCurHealth()) + "/" + std::to_string(enemy->getMaxHealth()));
    playerHealth.setString(std::to_string(player->getCurHealth()) + "/" + std::to_string(player->getMaxHealth()));
    enemyEffect.setString(enemy->getStatus()->getEffectHandle());
    playerEffect.setString(player->getStatus()->getEffectHandle());
}
