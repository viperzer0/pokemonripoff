cmake_minimum_required(VERSION 2.6)
project(PokemonRipOff)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11 -ggdb")
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY "../")

set(VERSION_MAJOR 1)
set(VERSION_MINOR 0)

configure_file (
    "${PROJECT_SOURCE_DIR}/config.h.in"
    "${PROJECT_BINARY_DIR}/config.h"
    )

#Tell CMake where to find the module.
set(CMAKE_MODULE_PATH ${CMAKE_CURRENT_SOURCE_DIR}/cmake)
#Look for SFML

find_package(SFML REQUIRED COMPONENTS graphics window system audio)

set(MY_SOURCE_FILES
    src/main.cpp
    src/GameInit.cpp
    src/GameEvents.cpp
    src/GameLoop.cpp
    src/GameRender.cpp
    src/GameClose.cpp
    src/AnimateSprite.cpp
    src/Cursor.cpp
    src/Moves.cpp
    src/handleCMDInput.cpp
    src/fighterFromString.cpp
    src/Status/DisableStatus.cpp
    src/Status/DreadStatus.cpp
    src/Status/PoisonStatus.cpp
    src/Status/StallStatus.cpp
    src/Fighter/Fighter.cpp
    src/Fighter/DummyFighter.cpp
    src/Fighter/PolarFighter.cpp
    src/Fighter/EliFighter.cpp
    src/Fighter/KTFighter.cpp
    src/Fighter/GinniFighter.cpp
    src/Fighter/JoshFighter.cpp
    src/Fighter/ZionFighter.cpp
)

include_directories(${SFML_INCLUDE_DIR})
include_directories(${PROJECT_BINARY_DIR})
#Define the target
add_executable(PokemonRipOff ${MY_SOURCE_FILES})
#Link SFML and dependencies
target_link_libraries(PokemonRipOff ${SFML_LIBRARIES} ${SFML_DEPENDENCIES})

